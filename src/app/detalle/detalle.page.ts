import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CitisService, CargaDescarga, CargaDescargaDetalle } from '../api/modulos/citis/citis.service';
import { UtileriaService } from '../api/utileria/utileria.service';
import { IonInput, LoadingController, NavController } from '@ionic/angular';
import { ModalController, Platform } from '@ionic/angular';


import {
  BarcodeScanner,
  Barcode
} from "@capacitor-mlkit/barcode-scanning";
import { DialogService } from '../core';
import { BarcodeScanningModalComponent } from './detalle-modal.component';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {
  tipo:number;
  codigo: string = "";
  cargaDescarga: CargaDescarga = new CargaDescarga();
  @ViewChild('q') public input: IonInput;
  cameraClass  = "";
  isConsulta: boolean = false;
  back:boolean = false;

  constructor(
    public route:ActivatedRoute,
    public citisService: CitisService,
    public utileriaService: UtileriaService,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private readonly dialogService: DialogService,
    public platform : Platform

    //private barcodeScanner: BarcodeScanner,

  ) { 
    
    this.route.queryParams.subscribe(params=>{
      //this.cargaDescarga = new CargaDescarga();
      //this.cargaDescargaDetallesSend = [];
      console.log(params);
       this.tipo = params["tipo"];
       this.cargaDescarga.id = params["id"];
       this.cargaDescarga.tipo = params["tipo"];
       this.cargaDescarga.codigo_camion =  params["codigo_camion"];
       this.cargaDescarga.operador = params["operador"];
       this.cargaDescarga.sucursal = params["sucursal"];
       this.isConsulta = params["isConsulta"];
    });

    /*this.platform.backButton.subscribeWithPriority(10, () => {
      console.log('Handler was called!');
        BarcodeScanner.stopScan();
        document.querySelector('body')?.classList.remove('barcode-scanning-active');
    });*/
  }

  ngOnInit() {
    if(this.isConsulta){
      try {
            //Creamos un modal de Carga
      this.loadingCtrl.create({
        spinner:'circular',
        message:'Guardando ....',
      }).then(loading =>{
        //Mostramos el modal de carga
        loading.present();
        //Consultamos la carga por su ID
        this.citisService.getId(this.cargaDescarga.id).then((listaCargaDescarga:any)=>{
          if(this.utileriaService.validarWCF(listaCargaDescarga)){
            let respuesta = JSON.stringify(listaCargaDescarga.data);
  
            console.log(respuesta);
            //Asignamos a nuestra variable global la respuesta. 
            this.cargaDescarga = JSON.parse(respuesta);
            loading.dismiss();
          }else{
            loading.dismiss();
          }
        })
      });
  
      } catch (error) {
        this.utileriaService.MessageBox("ERROR","Ocurrio un error fatal","alertDanger",0);
      }
    }

  }

  public async scan(): Promise<void> {
    const element = await this.dialogService.showModal({
      component: BarcodeScanningModalComponent,
      cssClass: 'barcode-scanning-modal',
      showBackdrop: false,
      componentProps: {
        formats: ["CODE_128","QR_CODE"],
        lensFacing: "BACK",
      },
    });
    element.onDidDismiss().then((result) => {
      const barcode: Barcode | undefined = result.data?.barcode;
      if (barcode) {
        this.agregarCaja(barcode.rawValue)
      }
    });
  }
  
  /*async scan (){
    return new Promise(async resolve => {
      document.querySelector("body")?.classList.add("barcode-scanning-active");
      const listener = await BarcodeScanner.addListener(
        'barcodeScanned', 
        async (result:any) => {
          await listener.remove();
          await BarcodeScanner.stopScan();
          document.querySelector('body')?.classList.remove('barcode-scanning-active');

          console.log("Esto Arroja el Result", result);
          console.log("Esto Arroja el Result JSON", result.barcode.rawValue);

          this.agregarCaja(result.barcode.rawValue)
          resolve(true) 
        },
      );
      await BarcodeScanner.startScan();
    });
  }*/
  
  agregarCaja(text:any){
    let detalle: CargaDescargaDetalle ={
      id: 0,
      id_registro:0,
      codigo_producto : text,
      fecha_creacion :""
    }

      this.cargaDescarga.detalles.push(detalle);
      this.input.value = "";
      this.input.setFocus();
  }

  Eliminar(element: CargaDescargaDetalle, index: number){
    this.utileriaService.MessageBoxYesNo("Eliminar","¿Deseas eliminar el registro "+element.codigo_producto+" de la captura de registros ?").then((bandera:boolean)=>{
      if(bandera){
        if(this.isConsulta){
          if(element.id == 0){ 
            this.utileriaService.MessageBox("ELIMINADO","Registro "+element.codigo_producto+" Eliminado Correctamente","alertSuccess",0);
            this.cargaDescarga.detalles.splice(index,1)
          }else{
            //Creamos un modal de Carga
            this.loadingCtrl.create({
              spinner:'circular',
              message:'Eliminando Registro ....',
            }).then(loading =>{
              //Mostramos el modal de carga
              loading.present();
              //eliminacion con api.
              this.citisService.delete(this.cargaDescarga.id,element.id).then(data=>{
                if(this.utileriaService.validarWCF(data)){
                  loading.dismiss();
                  this.utileriaService.MessageBox("ELIMINADO","Registro "+element.codigo_producto+" Eliminado Correctamente","alertSuccess",0);
                  this.cargaDescarga.detalles.splice(index,1)
                }else{
                  loading.dismiss();
                }
              });
            });
          }
        }else{
          //eliminacion directo al array
          this.utileriaService.MessageBox("ELIMINADO","Registro "+element.codigo_producto+" Eliminado Correctamente","alertSuccess",0);
          this.cargaDescarga.detalles.splice(index,1)
        }
      }
    });
  }

  Guardar(){
    //Creamos un modal de Carga
    this.loadingCtrl.create({
      spinner:'circular',
      message:'Guardando ....',
    }).then(loading =>{
      //Mostramos el modal de carga
      loading.present();
      //Filtramos para enviar solamente las etiquetas agregadas despues de editar
      this.cargaDescarga.detalles = this.cargaDescarga.detalles.filter(x=>x.id == 0);
      if(this.isConsulta){
        this.citisService.put(this.cargaDescarga).then(cargaDescarga=>{ 
          if(this.utileriaService.validarWCF(cargaDescarga)){
            //Desaparecemos nuestro modal de carga
            loading.dismiss().then(()=>{
              //enviamos un mensaje de EXITO
              this.utileriaService.MessageBox("EXITO",`${this.tipo==0 ? 'Carga':'Descarga'} Guardado Correctamente `,"alertSuccess",0);
  
              //Cerramos  la pagina actual
              this.navCtrl.pop();
        
            });
          }else loading.dismiss();
        });
      }else{
        this.citisService.post(this.cargaDescarga).then(cargaDescarga=>{ 
          if(this.utileriaService.validarWCF(cargaDescarga)){
            //Desaparecemos nuestro modal de carga
            loading.dismiss().then(()=>{
              //enviamos un mensaje de EXITO
              this.utileriaService.MessageBox("EXITO",`${this.tipo==0 ? 'Carga':'Descarga'} Guardado Correctamente `,"alertSuccess",0);
  
              //Cerramos  la pagina actual
              this.navCtrl.pop();
        
            });
          }else loading.dismiss();
        });
      }

    });

  }



}
