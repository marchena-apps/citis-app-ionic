import { Injectable } from '@angular/core';
import { CapacitorHttp, HttpOptions } from '@capacitor/core';

@Injectable({
  providedIn: 'root'
})
export class CitisService {
  //private path = "http://localhost:9000"
  //private path = "http://10.30.51.164:9000"
  private path = "http://104.236.69.232:9000"
  constructor() { }

    get(){
      return new Promise(resolve =>{
        const options = {
          url: `${this.path}/api/v1/cargaydescarga`,
          headers: {},
          params: {},
        };

        CapacitorHttp.get(options).then(data => {
          console.log(data.status);
          console.log(data.data); // data received by server
          console.log(data.headers);
          resolve(data);
        })
        .catch(error => {
          console.log(error.status);
          console.log(error.error); // error message as string
          console.log(error.headers);2
          resolve(error);
        });

      });
  }

  getId(id_cargaydescarga: number){
    return new Promise(resolve =>{
      const options = {
        url: `${this.path}/api/v1/cargaydescarga/${id_cargaydescarga}`,
        headers: {},
        params: {},
      };
    
      CapacitorHttp.get(options).then(data => {
        console.log(data.status);
        console.log(data.data); // data received by server
        console.log(data.headers);
        resolve(data);
      })
      .catch(error => {
        console.log(error.status);
        console.log(error.error); // error message as string
        console.log(error.headers);
        resolve(error);
      });

    });
  }

  post(CargaYDescarga: CargaDescarga){
    return new Promise(resolve =>{
      const options : HttpOptions = {
        url: `${this.path}/api/v1/cargaydescarga`,
        headers: {
          "Accept":"application/json",
          "Content-Type": "application/json",
        },
        method: 'POST',
        data: {registro:CargaYDescarga},
       
      };
    
      CapacitorHttp.post(options).then(data => {
        console.log(data.status);
        console.log(data.data); // data received by server
        console.log(data.headers);
        resolve(data);
      })
      .catch(error => {
        console.log(error.status);
        console.log(error.error); // error message as string
        console.log(error.headers);
        resolve(error); 
      });

    });

    
  }

  put(CargaYDescarga: CargaDescarga){
    return new Promise(resolve =>{
      const options : HttpOptions = {
        url: `${this.path}/api/v1/cargaydescarga/${CargaYDescarga.id}`,
        headers: {
          "Accept":"application/json",
          "Content-Type": "application/json",
        },
        method: 'PUT',
        data: {registro:CargaYDescarga},
       
      };
    
      CapacitorHttp.put(options).then(data => {
        console.log(data.status);
        console.log(data.data); // data received by server
        console.log(data.headers);
        resolve(data);
      })
      .catch(error => {
        console.log(error.status);
        console.log(error.error); // error message as string
        console.log(error.headers);
        resolve(error); 
      });

    });

    
  }
  
  delete(Id_cargaDescarga:number, Id_producto: number){
    return new Promise(resolve =>{
      const options = {
        url: `${this.path}/api/v1/cargaydescarga/${Id_cargaDescarga}/detalles/${Id_producto}`,
        headers: {},
        params: {},
      };
    
      CapacitorHttp.delete(options).then(data => {
        console.log(data.status);
        console.log(data.data); // data received by server
        console.log(data.headers);
        resolve(data);
      })
      .catch(error => {
        console.log(error.status);
        console.log(error.error); // error message as string
        console.log(error.headers);
        resolve(error);
      });

    });
}

}


@Injectable({
  providedIn: 'root'
})
export class CargaDescarga{
  id: number;
  codigo_camion: string;
  operador: string;
  tipo: number;
  sucursal: string;
  fecha_creacion: string;
  id_registro: number;
  contador:number;
  detalles: CargaDescargaDetalle[]
  constructor(){
    this.id = NaN;
    this.codigo_camion = "";
    this.operador ="";
    this.tipo = 0;
    this.sucursal = "";
    this.fecha_creacion = "";
    this.id_registro = 0;
    this.contador = 0;
    this.detalles = [];
  }
}

@Injectable({
  providedIn: 'root'
})
export class CargaDescargaDetalle{
  id: number;
  id_registro: number;
  codigo_producto: string;
  fecha_creacion: string;
  constructor(){
    this.id = NaN;
    this.codigo_producto = "";
    this.fecha_creacion = "";
    this.id_registro = 0;
  }
}


