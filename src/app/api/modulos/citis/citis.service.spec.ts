import { TestBed } from '@angular/core/testing';

import { CitisService } from './citis.service';

describe('CitisService', () => {
  let service: CitisService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CitisService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
