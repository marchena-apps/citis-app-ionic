import { Injectable } from '@angular/core';
import { AlertController, IonicSafeString } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UtileriaService {

  constructor(   public alertCtrl: AlertController) { }

    /**
   * @data Respuesta del Servicio HTTP
   * @Info Retorna un mensaje dependiendo el Status de la respuesta HTTP del servicio consumido Valida si fue correcto o no.
   */
  public validarWCF(data:any){
    if(data.status == 400){
      this.MessageBox("ERROR",`Ocurrio un problema ${data["headers"]["x-android-response-source"]}`,"alertDanger",0);
      return false;
    }
    else if(data.status == 404){
      this.MessageBox("ERROR",`Ocurrio un problema ${data["error"]}`,"alertDanger",0);
      return false;
    }
    else if(data.status == -1){
      this.MessageBox("ERROR",`Ocurrio un problema ${data["error"]}`,"alertDanger",0);
      return false;
    }
    else if(data.status == -4){
      this.MessageBox("ERROR",`Ocurrio un problema TIME OUT`,"alertDanger",0);
      return false;
    }
    else if(data.status == 500){
      console.log(data);
      let objeto: any = JSON.parse(data.error);
      this.MessageBox("ERROR",`Ocurrio un problema, ${objeto["Mensaje"]} ${data["headers"]["x-android-response-source"]}`,"alertDanger",0);
      return false;
    }else{
      return true;
    }
  }

    /**
   * @subTitle es el titulo de la alerta
   * @mensaje es el cuerpo del mensaje
   * @type 0 Para que el mensaje sea persistente 1 para que no sea persistente.
   * @class es la clase son alertInfo alertSuccess alertDanger
   * @Info Dispara un Message Box informativo.
   */
    MessageBox(subtitle:string,message:string, classs:string, type:number){
      this.alertCtrl.create({
            header: "",
            //subHeader: subtitle == "" ? "" : subtitle,
            subHeader: subtitle,
            message: message == "" ? "": message,
            backdropDismiss: type==0 ? false : true,
            buttons: [
              {
                text:"OK",
                handler:data=>{
                  console.log("Solo soy una imagen")
                },
                role:'cancel'
              },
        ],
        cssClass: classs,
      }).then(alert=>{
        alert.present();
      });
    }

        /**
   * @subTitle es el titulo de la alerta
   * @mensaje es el cuerpo del mensaje
   * @class es la clase son alertInfo alertSuccess alertDanger
   * @Info Dispara un Message Box informativo el cual espera una respuesta Afirmativa o Negativa.
   */
    MessageBoxYesNo(subTitle:string, mensaje: string){
      return new Promise(res => {
        
        this.alertCtrl.create({
          header: "",
          subHeader: subTitle,
          message:  mensaje,
          backdropDismiss: false,
          cssClass: "alertInfo",
          buttons:[
            {
              text: 'NO',
              handler: data =>{
                res(false);
                console.log("NO");
              },
              role:''
            },
            {
              text: "SI",
              handler: data =>{
                res(true);
                console.log("REGRESO SI");
              },
              role:'cancel'
            }
          ]
        }).then(alert=>{
          alert.present();
        });

      });  
    }

   /**
   * @subTitle es el titulo de la alerta
   * @mensaje es el cuerpo del mensaje
   * @class es la clase son alertInfo alertSuccess alertDanger
   * @Info Dispara un Message Box informativo el cual espera una respuesta de confirmacion.
   */
    MessageBoxEsperaOK(subTitle:string, mensaje: string, classs:string){
      return new Promise(res => {
        this.alertCtrl.create({
          header: "",
          subHeader: subTitle,
          message: mensaje == "" ? "": mensaje,
          backdropDismiss: false,
          cssClass: classs,
          buttons:[
            {
              text: "OK",
              handler: data =>{
                res(true);
                console.log("REGRESO OK");
              },
              role:'cancel'
            }
          ]
        }).then(alert=>{
          alert.present();
        });
  
      });  
    }
}
