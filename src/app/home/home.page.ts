import { Component, ViewChild } from '@angular/core';
import { IonModal, NavController, ModalController, LoadingController } from '@ionic/angular';
import { UtileriaService } from '../api/utileria/utileria.service';
import { CitisService, CargaDescarga } from '../api/modulos/citis/citis.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public operacion: CargaDescarga = new CargaDescarga();
  @ViewChild("modaloc") modaloc : IonModal;
  public listaCargaDescarga: CargaDescarga[] = []

  constructor(
    public utileriaService: UtileriaService,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public citisService: CitisService,
    public loadingCtrl: LoadingController
    
    ) {}

  ionViewDidEnter(){
    //Cada vez que entremos a la pagina generara la busqueda.
    this.ngAfterViewInit();
  }
  ngAfterViewInit() {
    try {
      this.loadingCtrl.create({
        spinner:'circular',
        message:'Guardando ....',
      }).then(loading =>{
        //Mostramos el modal de carga
        loading.present();
        this.citisService.get().then((listaCargaDescarga:any)=>{
          if(this.utileriaService.validarWCF(listaCargaDescarga)){
            let respuesta = JSON.stringify(listaCargaDescarga.data);
  
            console.log(respuesta);
            this.listaCargaDescarga = JSON.parse(respuesta);
            loading.dismiss();
          }else{
            loading.dismiss();
          }
        })
      });

    } catch (error) {
      this.utileriaService.MessageBox("ERROR","Ocurrio un error fatal","alertDanger",0);
    }

  }

  modificar(element:CargaDescarga){
    this.navCtrl.navigateForward("/detalle",{queryParams:{
      id:element.id, 
      operador: String(element.operador), 
      sucursal:  String(element.sucursal), 
      codigo_camion:  String(element.codigo_camion) ,
      tipo:element.tipo,
      isConsulta: true
    }});
  }
  create(){
    //Mostramos el modal donde agregamos la info
    this.modaloc.present();  
  }

  generarOperacion(type:number){
    if(this.operacion.codigo_camion == ""){
      this.utileriaService.MessageBox("ERROR","Es necesario agregar el codigo del camion","alertDanger",0);
      return;
    }
    if(this.operacion.operador == ""){
      this.utileriaService.MessageBox("ERROR","Es necesario agregar el operador","alertDanger",0);
      return;
    }
    if(this.operacion.sucursal == ""){
      this.utileriaService.MessageBox("ERROR","Es necesario agregar la sucursal","alertDanger",0);
      return;
    }




    //llamamos al metodo para cerrar el modal.
    this.close();
    //Nos envia a la pagina del detalle en modo capa
    this.navCtrl.navigateForward("/detalle",{queryParams:{
      id:0, 
      operador: String(this.operacion.operador), 
      sucursal:  String(this.operacion.sucursal), 
      codigo_camion:  String(this.operacion.codigo_camion) ,
      tipo:type,
      isConsulta: false
    }});
    this.operacion  = new CargaDescarga();
  }
  close(){
    //Desaparecemos el modal donde agregamos la info
    this.modaloc.dismiss();

  }

}
