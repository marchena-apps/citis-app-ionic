<h1 align="center">Desarrollo Express De Aplicaciones Moviles Con Ionic Framework</h1>
<h1 align="center">CITIS 2023</h1>
<h2 align="lefth">
Autor: Jesus Ernesto Marchena Sanchez <br>
Empresa: Administradora y consultora de Negocios del Pacífico (GRUPO PINSA)<br>
Puesto: Ingeniero de Software
</h2>

# INSTALACION DE RECURSOS
>LINK DE DESCARGA <br>
>https://core.fimaz.uas.edu.mx/citis2023/ionic/<br>
>Instalamos android Studios<br>
>Entramos a las configuraciones del SDK e instalamos ANDROID API del 29 - 34<br>
>Instalamos java 17<br>
>Instalamos NODE JS 18.18.0<br>
> Agregamos Variables de sistema<br>
>JAVA_HOME: C:\Program Files\Java\jdk-17<br>
>ANDROID_HOME: C:\Users\ejemplo\AppData\Local\Android\Sdk (esta ruta la consigues en las configuraciones de SDK de android studios)<br>
>En la variable path de sistema agregamos las siguientes rutas<br>
>%JAVA_HOME%\bin<br>
>%ANDROID_HOME%\platform-tools<br>


# URL DEBUG 
>cuando estemos debugeando nos ayudara a revisar los logs de nuestra aplicacion.
>`chrome://inspect/#devices`

# PASO 1
>Abrimos CMD y ejecutamos `npm i -g @ionic/cli` <br>
>Creamos el proyecto IONIC con el comando: `ionic start [Nombe del proyecto] blank` <br>
En `[Nombre del proyecto]` asignamos un nombre por ejemplo `ionic start citis blank`

# PASO 2
>Seleccionamos el lenguaje deseado  `Angular, React o Vue` Seleccionaremos `Angular`

# PASO 3
>ejecutamos el comando `ionic build`<br>
>Probamos nuestro proyecto ejecutando el comando `ionic serve`

# PASO DE CONFIGURACION ANEXAR RECURSOS MANUALMENTE
>RECURSOS `https://core.fimaz.uas.edu.mx/citis2023/ionic/`<br>
>En la Uri de recursos encontraremos un archivo llamado `RECUSROS.zip` la carpeta `img` la agregamos a `/assets` 

>En el archivo `capacitor.config.ts` agregaremos la siguiente configuración para cambiarle el app ID de nuestra app y el nombre de nuestra aplicacion movil
 ```
 const config: CapacitorConfig = {
  appId: 'cargaydescargacitis.uas.com',
  appName: 'Carga y Descarga Citis',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};
 ```

# PASO 4
>Si no esta generado el archivo `www` Corremos el siguiente comando `ionic build`
>Ya que corrio todo excelente y visualizamos nuestra pantalla en blanco procedemos a crear el proyecto ANDROID con el siguiente comando `ionic capacitor add android`



# PASO 5
>Remplazamos el `<ion-header></ion-header>` Actual por el siguiente fragmento de codigo  en el archivo `home.page.html`.
``` 
<ion-header [translucent]="true">
  <ion-toolbar class="tema-principal">
    <ion-title>
      Carga & Descarga
    </ion-title>
  </ion-toolbar>
</ion-header>
``` 
>Diseño de la aplicacion movil anexamos el siguiente codigo en la pantalla de Home en el archivo `home.page.html`. dentro de las etiquetas <br> ```<ion-content></ion-content>```
``` 
<ion-card>
    <ion-card-header>
      <ion-card-title>MGH2183239</ion-card-title>
    </ion-card-header>
    <ion-card-content>
      <ion-grid>
        <ion-row>
          <ion-col>
            50 Paquetes
          </ion-col>
          <ion-col>
            MAZATLAN SINALOA
          </ion-col>
        </ion-row>
        <ion-row>
          <ion-col>
            Gabriel Sanchez Ortega
          </ion-col>
          <ion-col>
            <b>Carga</b>
          </ion-col>
        </ion-row>
      </ion-grid>
    </ion-card-content>
    <div [ngClass]="'cargaOdescarga'" ></div>
</ion-card>
```
`RESULTADO`<br>
![A si quedaria implementando el codgo Card](https://lh3.googleusercontent.com/drive-viewer/AITFw-xnXoG1RHrbyTr7NM7NYXwqvo83SUcumtFefikLMeSVddq3Ku6AYvUr3Ay9DyUlN0odjR2GSlqSVDQbtDUcVMeiU11q=w1366-h619 "Implementacion de Card")

# PASO 6
>Anexamos un button tipo Fab anexamos el siguiente codigo
```
  <ion-fab class="tema-principal"  slot="fixed" vertical="bottom" horizontal="center">
    <ion-fab-button  class="tema-principal"> 
      <ion-icon name="add-circle-outline"></ion-icon>    
    </ion-fab-button>
  </ion-fab>
```
`RESULTADO`<br>
![A si quedaria implementando el codgo Card](https://lh3.googleusercontent.com/drive-viewer/AITFw-wxfaww8BkVx0wjsPyyQH5YbP77IR0YQs4RTIiI5iZjVAccyhBEYofH8kL6mioBgJT7_x_1aLOv0W9kpD2_yy55VmfzKg=w1366-h619 "Implementacion de FAB Button")

# PASO 7
>Preparamos el Tipado de los objetos que manejaremos para el Formulario generando un service para consultarlo y extraerlo en cualquiera de nuestras pantallas ejecutamos el siguiente comando en CONSOLA si tu proyecto esta corriendo con `ionic serve` dentenlo haciendo `CTRL+C` y ejecuta el comando.
`ionic g service api/modulos/citis`

# PASO 8
>En los recursos que les brinde agregue un service llamado `utileria` ya construido lo anexamos manualmente a la carpeta que creamos de `/api` ingresamos la carpeta completa de `utileria` este Service contiene accesos a `MessagesBox` que nos ayudaran mas a delante a interactuar e informar al usuario facilmente de sucesos dentro de la app incluso nos ayudara a validar cualquier `API` con las respuestas o errores que nos llegue a retornar.


# Paso 9
>Una vez agregado anexamos el siguiente codigo dentor del archivo `api/modulos/citis.service.ts` al finalizar la ultima llave, Con esto generamos el tipado de la informacion de la api que cosultaremos más adelante.

```
@Injectable({
  providedIn: 'root'
})
export class CargaDescarga{
  id: number;
  codigo_camion: string;
  operador: string;
  tipo: number;
  sucursal: string;
  fecha_creacion: string;
  id_registro: number;
  contador:number;
  detalles: CargaDescargaDetalle[]
  constructor(){
    this.id = NaN;
    this.codigo_camion = "";
    this.operador ="";
    this.tipo = 0;
    this.sucursal = "";
    this.fecha_creacion = "";
    this.id_registro = 0;
    this.contador = 0;
    this.detalles = [];
  }
}

@Injectable({
  providedIn: 'root'
})
export class CargaDescargaDetalle{
  id: number;
  id_registro: number;
  codigo_producto: string;
  fecha_creacion: string;
  constructor(){
    this.id = NaN;
    this.codigo_producto = "";
    this.fecha_creacion = "";
    this.id_registro = 0;
  }
}

```

# PASO 10
>Agregar Formulario para crear un registro con un MODAL, anexamos el siguiente codigo de antes de la etiqueta `</ion-content>` Es posible que despues de agregarlo marque error ya que declaramos un objeto con variables y metodos que no existen dentro de nuestro `home.page.ts` que es el controlador de nuestra ventana o pagina. si marca error no te asustes continua con los pasos y se arreglara.

```
<!-- MODAL RECEPCION ITEM-->
 <ion-modal #modaloc trigger="open-modal-recepcion" [canDismiss]="true" [presentingElement]="false" [initialBreakpoint]="0.75" [breakpoints]="[0, 0.25, 0.5, 0.75]">
      <ng-template >
        <ion-header>
          <ion-toolbar>
            <ion-title>Carga o Descarga</ion-title>
            <ion-buttons slot="end">
              <ion-button (click)="close()"  color="danger" >Cerrar</ion-button>
            </ion-buttons>
          </ion-toolbar>
        </ion-header>
  
        <ion-content >
          <ion-grid>
            <ion-row>
              <ion-item class="input-custom">
                <!-- [(ngModel)]="operacion.codigo_camion"-->
                <ion-input class="input-custom input-center" [(ngModel)]="operacion.codigo_camion" placeholder="Codigo Camion" type="text" disabled="false"></ion-input>
              </ion-item>
            </ion-row>
            <ion-row>
              <ion-item class="input-custom ">
                <!-- [(ngModel)]="operacion.operacion"-->
                <ion-input class="input-custom input-center" [(ngModel)]="operacion.operador" placeholder="Operador" type="text" disabled="false"></ion-input>
              </ion-item>
            </ion-row>
            <ion-row>
              <ion-item class="input-custom ">
                <ion-label  position="floating">Sucursal:</ion-label>
                <!--[(ngModel)]= "operacion.sucursal" [value]="operacion.sucursal"-->
                <ion-select  class="input-custom" [(ngModel)]="operacion.sucursal" [value]="operacion.sucursal" placeholder="--Selecciona una sucursal--" >
                  <ion-select-option   value="Mazatlan">Mazatlan</ion-select-option>
                  <ion-select-option   value="Culiacan">Culiacan</ion-select-option>
                  <ion-select-option   value="Los Mochis">Los Mochis</ion-select-option>
                  <ion-select-option   value="Guasave">Guasave</ion-select-option>                  
                </ion-select>
              </ion-item>
            </ion-row>

            <ion-row class="items-center">
              <ion-item  >
                <ion-button (click)="generarOperacion(0)" color="success">
                  <img slot="icon-only" src="../../assets/img/box64.png">
                  CARGA
                </ion-button>
                <ion-button  (click)="generarOperacion(1)"  color="danger">
                  <img src="../../assets/img/box64.png">
                  DESCARGA
                </ion-button>
              </ion-item>
            </ion-row>
          </ion-grid>
        </ion-content>
      </ng-template>
</ion-modal>
```
# PASO 11
>Creamos las variables utilizadas en el modal agregado recientemente antes del `constructor(){}` agregamos las siguientes lineas de codigo.

```
  public operacion: CargaDescarga = new CargaDescarga();
  @ViewChild("modaloc") modaloc : IonModal;
```
# PASO 12
>Importamos los Service que necesitamos en la pagina de `home.page.ts` y anexamos un service más que es el `ViewChild` este nos ayuda a interactuar con elementos DOM del sitio HTML lo agregamos en el inicio del documento.

```
import { Component, ViewChild } from '@angular/core';
import { IonModal, NavController, ModalController, LoadingController } from '@ionic/angular';
import { UtileriaService } from '../api/utileria/utileria.service';
import { CitisService, CargaDescarga } from '../api/modulos/citis/citis.service';
```
>Seguido de importar los declaramos en el constructor de la pagina `home.page.ts`
```
  constructor(
    public utileriaService: UtileriaService,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public citisService: CitisService,
    public loadingCtrl: LoadingController
    
    ) {}
```

# PASO 13
>Una vez importado y declarado en el contructor todos los elemntos procedemos a agregar los metodos que nos faltan en nuestro `home.page.ts` anexamos la siguiente linea de codigo despues del metodo del `constructor(){}`
```
  create(){
    //Mostramos el modal donde agregamos la info
    this.modaloc.present();  
  }

  generarOperacion(type:number){
    if(this.operacion.codigo_camion == ""){
      this.utileriaService.MessageBox("ERROR","Es necesario agregar el codigo del camion","alertDanger",0);
      return;
    }
    if(this.operacion.operador == ""){
      this.utileriaService.MessageBox("ERROR","Es necesario agregar el operador","alertDanger",0);
      return;
    }
    if(this.operacion.sucursal == ""){
      this.utileriaService.MessageBox("ERROR","Es necesario agregar la sucursal","alertDanger",0);
      return;
    }

    //llamamos al metodo para cerrar el modal.
    this.close();
    //Nos envia a la pagina del detalle en modo capa
    this.navCtrl.navigateForward("/detalle",
    {
        queryParams:{
            id:0, 
            operador: this.operacion.operador, 
            sucursal: this.operacion.sucursal, 
            codigo_camion: this.operacion.codigo_camion ,
            tipo:type,
            isConsulta: false
        }
    });
  }

  close(){
    //Desaparecemos el modal donde agregamos la info
    this.modaloc.dismiss();
  }
```
>En este punto nuestra pagina ya tiene interaccion. los botones Carga y Descarga marcaran error ya que aun no creamos la pagina llamada `detalle`

>Agregamos una de las funciones que agregamos recientemente al Boton FAB BUTTON le anexamos la funcion `(click)="create()"` en la etiqueta `<ion-fab-button>` quedaria de la siguiente manera.
```
  <ion-fab class="tema-principal"  slot="fixed" vertical="bottom" horizontal="center">
    <ion-fab-button  class="tema-principal" (click)="create()"  > 
      <ion-icon name="add-circle-outline"></ion-icon>    
    </ion-fab-button>
  </ion-fab>
```
`RESULTADO`<br>
![A si quedaria implementando el codgo Card](https://lh3.googleusercontent.com/drive-viewer/AITFw-wlKFo7I1z09ouhh42Y6hVnj8zQYso_kqH2QJXOcjcjNEkn8roNNo0MC3IH5syXGcDpIuwz_NC35d2Prx1lYmHT31fdSQ=w1366-h619 "Modal")


# PASO 14
>Ejecutamos el siguiente comando en CONSOLA si tu proyecto esta corriendo con `ionic serve` dentenlo haciendo `CTRL+C` y ejecuta el comando. `ionic g page detalle` y creata una nueva pantalla.

# PASO 15
>Importaremos las libreriras que necesitaremos para esta pantalla las agregamos al inicio del documento en la pagina  `detalle.page.ts`
```
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CitisService, CargaDescarga, CargaDescargaDetalle } from '../api/modulos/citis/citis.service';
import { UtileriaService } from '../api/utileria/utileria.service';
import { IonInput } from '@ionic/angular';
```
>Asignamos las variables que utilizaremos en la patalla `detalle.page.ts` lo agregamos antes del `constructor(){}`

```
  tipo:number;
  codigo: "";
  cargaDescarga: CargaDescarga = new CargaDescarga();
  cargaDescargaDetallesSend: CargaDescargaDetalle[] = [];
  @ViewChild('q') public input: IonInput;
```

>Preparamos la resepcion de variables de la pantalla anterior. en la pagina  `detalle.page.ts` en el apartado del `contructor(){}` agregaremos el siguiente codigo aprovecharemos y agregaremos tambien la declaracion de los servicios

```
  constructor(
    public route:ActivatedRoute,
    public citisService: CitisService,
    public utileriaService: UtileriaService
  ) { 
    
    this.route.queryParams.subscribe(params=>{
       this.tipo = params["tipo"];
       this.cargaDescarga.id = params["id"];
       this.cargaDescarga.tipo = params["tipo"];
       this.cargaDescarga.codigo_camion =  params["codigo_camion"];
       this.cargaDescarga.operador = params["operador"];
       this.cargaDescarga.sucursal = params["sucursal"];
       this.isConsulta = params["isConsulta"];
    });
  }

```
>De esta manera tiene que quedar el constructor

# PASO 16
>Remplazamos `<ion-header>` actual por el siguiente codigo 
```
<ion-header [translucent]="true">
  <ion-toolbar [ngClass]="tipo ==0 ? 'tema-carga' : 'tema-descarga'">
    <ion-buttons slot="start">
      <ion-back-button text="BACK" icon="caret-back"></ion-back-button>
    </ion-buttons>
    <ion-title>{{tipo == 0 ? 'CARGA' : 'DESCARGA'}}</ion-title>
  </ion-toolbar>
</ion-header>
```
>Agregamos en `<ion-content>` Para obtener una seccion Informativa

```
<ion-content scrollbar="true" [fullscreen]="true">
  <ion-card class="card-size">
    <ion-card-header >
      <ion-card-title>Camion: {{cargaDescarga.codigo_camion}}</ion-card-title>
    </ion-card-header>
    <ion-card-content>
      <ion-grid>
        <ion-row>
          <ion-col>
            {{cargaDescarga.sucursal}}
          </ion-col>
        </ion-row>
        <ion-row>
          <ion-col>
            Operador: {{cargaDescarga.operador}}
          </ion-col>
        </ion-row>
      </ion-grid>
    </ion-card-content>
    <div [ngClass]="'cargaOdescarga'" ></div>
  </ion-card>
</ion-content>
```

`RESULTADO`<br>
![A si quedaria implementando el codgo Card](https://lh3.googleusercontent.com/drive-viewer/AITFw-x8UWk53V0WHoXcnL9UjCiaMNo2caQiCCEG0BSjUG1z0yAoZdivs2gQ98R3lpNCMKU8zIT93GZbJYWKLZBnuwbFtZjmhQ=w1366-h619 "Detalle")

# PASO 17
> Agregamos el Metodo Eliminar, agregarCaja y Grabar en el archivo `detalle.page.ts` despues del `constructor(){}` agregamos el siguiente codigo.

```


  agregarCaja(text:any){
    console.log("se detono esta wea", text);
    let detalle: CargaDescargaDetalle ={
      id: 0,
      id_registro:0,
      codigo_producto : text,
      fecha_creacion :""
    }

      this.cargaDescarga.detalles.push(detalle);
      this.input.value = "";
      this.input.setFocus();
  }

Eliminar(element: CargaDescargaDetalle, index: number){
  this.utileriaService.MessageBoxYesNo("Eliminar","¿Deseas eliminar el registro "+element.codigo_producto+" de la captura de registros ?").then((bandera:boolean)=>{
      if(bandera){
        if(this.isConsulta){
          if(element.id == 0){ 
            this.utileriaService.MessageBox("ELIMINADO","Registro "+element.codigo_producto+" Eliminado Correctamente","alertSuccess",0);
            this.cargaDescarga.detalles.splice(index,1)
          }else{
            //eliminacion con api.
          }
        }else{
          //eliminacion directo al array
          this.utileriaService.MessageBox("ELIMINADO","Registro "+element.codigo_producto+" Eliminado Correctamente","alertSuccess",0);
          this.cargaDescarga.detalles.splice(index,1)
        }
      }
    });

  }

  Guardar(){
    //Realizamos un filtro para enviar solamente las etiquetas con id 0
    //Por el momento lo dejamos Vacio
  }
```

>Seguido en el archivo `detalle.page.html` agregamos el siguiente codigo de bajo de la etiqueta `</ion-card>` antes de cerrar el  `</ion-content>`
```
    <ion-list class="input-custom input-center" >
      <ion-input class="input-custom input-center" #q label="Codigo" labelPlacement="floating" [(ngModel)]="codigo" (keyup.enter)="agregarCaja(q.value)" enterkeyhint="send"></ion-input>
    </ion-list>
    <ion-list>
      <ion-content style="height: 200px;" [scrollEvents]="true" class="Scroll">
        <ion-item-sliding *ngFor="let element of cargaDescarga.detalles; let i= index" >
          <ion-item>
            <ion-thumbnail slot="start">
              <img alt="Silhouette of mountains" src="../../assets/img/box64.png" />
            </ion-thumbnail>
            <p>
              {{element.codigo_producto}}
            </p>         
          </ion-item>
      
          <ion-item-options>
            <ion-item-option (click)="Eliminar(element, i)" color="danger">ELIMINAR</ion-item-option>
          </ion-item-options>
        </ion-item-sliding>
      </ion-content>
    </ion-list>
```
`RESULTADO`<br>
![A si quedaria implementando el codgo Card](https://lh3.googleusercontent.com/drive-viewer/AITFw-xOocwZNyZ8fH9-dw4_Vkzr8kkx1WLUWtQ7AZ8qKJmrzoL6tY98gT5q1JVX6K893-6wsoiJpVHQ3FeQuPZNyj-f_-S0=w1366-h619 "DetalleLIST")


>Hasta a qui nuestra aplicacion es 100% Funcional modo DEMO Ahora continuaremos agregandole funcionalidad con las APIS. Guardaremos y manipularemos la informacion a si como consultarla.



# INTEGRANDO APIS A NUESTRO PROYECTO

# API PASO 1
> Una vez que ya tenemos nuestra app lista para darle conexion y interaccion con la base de datos necesitamos hacer uso de nuestro `CAPACITOR` una libreria en especifo llamada `CapacitorHttp` que nos ayudara con las conexiones a APIS.

>En el archivo `capacitor.config.ts` agregaremos el siguiente codigo en el objeto `config`

```
 plugins: {
    CapacitorHttp: {
      enabled: true,
    },
  },
```
>Tiene que queda a si como el siguiente ejemplo
```

const config: CapacitorConfig = {
  appId: 'cargaydescargacitis.uas.com',
  appName: 'Carga y Descarga Citis',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  },
  plugins: {
    CapacitorHttp: {
      enabled: true,
    },
  },
};
```

# API PASO 2
>Es hora de agregar nuestra api a nuestro proyecto para poder consultarla <br>
>Estas son las apis y sus Respuestas <br>
>Ruta: http://104.236.69.232:9000 <br>
# APIS
`POST` [GRABAR CARGA O DESCARGA] http://localhost:9000/api/v1/cargaydescarga

`POST`
```
{
    "registro": {
        "codigo_camion": 21312,
        "operador": "JEsus Marchena APP 2 prueba",
        "tipo": 0,
        "sucursal": "MOCHIS",
        "detalles": [
            {
                "id": 0,
                "id_registro": 0,
                "fecha_creacion": "",
                "codigo_producto": "12938111"
            },
            {
                "id": 0,
                "id_registro": 0,
                "fecha_creacion": "",
                "codigo_producto": "12938112"
            }
        ]
    }
}
```
`RESULT`
```
{
    "Respuesta": {
        "id": 16
    }
}
```

`PUT` [AGREGAR MODIFICAR] http://localhost:9000/api/v1/cargaydescarga/16

`PUT`
```
{
    "registro": {
        "codigo_camion": 21312,
        "operador": "JEsus Marchena APP 2 prueba",
        "tipo": 0,
        "sucursal": "MOCHIS",
        "detalles": [
            {
                "id": 0,
                "id_registro": 0,
                "fecha_creacion": "",
                "codigo_producto": "12938111"
            },
            {
                "id": 0,
                "id_registro": 0,
                "fecha_creacion": "",
                "codigo_producto": "12938112"
            }
        ]
    }
}
```
`RESULT`
```
{
    "Respuesta": {
        "id": 16
    }
}
```

`GET` [OBTENEMOS UNA LISTA DE CARGAS Y DESCARGAS] http://localhost:9000/api/v1/cargaydescarga

```
[
    {
        "id": 1,
        "codigo_camion": "1231",
        "operador": "Jesus Marchena",
        "tipo": 0,
        "Sucursal": "Mazatlan",
        "fecha_creacion": "2023-08-29T21:54:27.000Z",
        "id_registro": 1,
        "contador": 7
    },
    {
        "id": 2,
        "codigo_camion": "1231",
        "operador": "Jesus 2",
        "tipo": 0,
        "Sucursal": "Mazatlan",
        "fecha_creacion": "2023-08-29T23:03:03.000Z",
        "id_registro": 2,
        "contador": 2
    },
]
```


`GET` [OBTENEMOS UN OBJETO ESPECIFICO DE CARGA Y DESCARGA] http://localhost:9000/api/v1/cargaydescarga/16

```
{
    "id": 16,
    "codigo_camion": "21312",
    "operador": "JEsus Marchena APP",
    "tipo": 0,
    "Sucursal": "MOCHIS",
    "fecha_creacion": "2023-09-11T18:51:59.000Z",
    "detalles": [
        {
            "id": 83,
            "id_registro": 16,
            "codigo_producto": "12938111",
            "fecha_creacion": "2023-09-11T18:51:59.000Z"
        },
        {
            "id": 84,
            "id_registro": 16,
            "codigo_producto": "12938112",
            "fecha_creacion": "2023-09-11T18:51:59.000Z"
        }
    ]
}
```






`DELETE` [ELIMINAMOS UN ARTICULO DE LA CARGA O DESCARGA] http://localhost:9000/api/v1/cargaydescarga/17/detalles/88


>En el archivo `api/modulos/citis/citis.service.ts` Agregaremos las siguientes lineas de codigo para integrar nuestra api a nuestro proyecto. Remplazamos el contenido dentro de `CitisService`

```
  private path = "http://10.30.51.164:9000"
  constructor() { }

    get(){
      return new Promise(resolve =>{
        const options = {
          url: `${this.path}/api/v1/cargaydescarga`,
          headers: {},
          params: {},
        };

        CapacitorHttp.get(options).then(data => {
          console.log(data.status);
          console.log(data.data); // data received by server
          console.log(data.headers);
          resolve(data);
        })
        .catch(error => {
          console.log(error.status);
          console.log(error.error); // error message as string
          console.log(error.headers);2
          resolve(error);
        });

      });
  }

  getId(id_cargaydescarga: number){
    return new Promise(resolve =>{
      const options = {
        url: `${this.path}/api/v1/cargaydescarga/${id_cargaydescarga}`,
        headers: {},
        params: {},
      };
    
      CapacitorHttp.get(options).then(data => {
        console.log(data.status);
        console.log(data.data); // data received by server
        console.log(data.headers);
        resolve(data);
      })
      .catch(error => {
        console.log(error.status);
        console.log(error.error); // error message as string
        console.log(error.headers);
        resolve(error);
      });

    });
  }

  post(CargaYDescarga: CargaDescarga){
    return new Promise(resolve =>{
      const options : HttpOptions = {
        url: `${this.path}/api/v1/cargaydescarga`,
        headers: {
          "Accept":"application/json",
          "Content-Type": "application/json",
        },
        method: 'POST',
        data: {registro:CargaYDescarga},
       
      };
    
      CapacitorHttp.post(options).then(data => {
        console.log(data.status);
        console.log(data.data); // data received by server
        console.log(data.headers);
        resolve(data);
      })
      .catch(error => {
        console.log(error.status);
        console.log(error.error); // error message as string
        console.log(error.headers);
        resolve(error); 
      });

    });

    
  }

   put(CargaYDescarga: CargaDescarga){
    return new Promise(resolve =>{
      const options : HttpOptions = {
        url: `${this.path}/api/v1/cargaydescarga/${CargaYDescarga.id}`,
        headers: {
          "Accept":"application/json",
          "Content-Type": "application/json",
        },
        method: 'PUT',
        data: {registro:CargaYDescarga},
       
      };
    
      CapacitorHttp.put(options).then(data => {
        console.log(data.status);
        console.log(data.data); // data received by server
        console.log(data.headers);
        resolve(data);
      })
      .catch(error => {
        console.log(error.status);
        console.log(error.error); // error message as string
        console.log(error.headers);
        resolve(error); 
      });

    });

    
  }
  
  delete(Id_cargaDescarga:number, Id_producto: number){
    return new Promise(resolve =>{
      const options = {
        url: `${this.path}/api/v1/cargaydescarga/${Id_cargaDescarga}/detalles/${Id_producto}`,
        headers: {},
        params: {},
      };
    
      CapacitorHttp.delete(options).then(data => {
        console.log(data.status);
        console.log(data.data); // data received by server
        console.log(data.headers);
        resolve(data);
      })
      .catch(error => {
        console.log(error.status);
        console.log(error.error); // error message as string
        console.log(error.headers);
        resolve(error);
      });

    });
}
```
>Importamos al inicio del documento nuestro api nativa de CAPACITOR `api/modulos/citis/citis.service.ts`

```
import { CapacitorHttp, HttpOptions } from '@capacitor/core';
```
# API PASO 3
>Regresaremos a la pagina de `home.page.html` en esta pagina la prepararemos para recibir una lista de informacion. el componente `<ion-card>` lo remplasaremos por el siguiente codigo

```
  <ion-list >
    <ion-item  *ngFor="let element of listaCargaDescarga; let i= index">
      <ion-card class="card-size">
        <ion-card-header>
          <ion-card-title>Camion:{{element.codigo_camion}}</ion-card-title>
        </ion-card-header>
        <ion-card-content>
          <ion-grid>
            <ion-row>
              <ion-col>
                Paquetes: {{element.contador}}
              </ion-col>
              <ion-col>
                Sucursal: {{element.sucursal}}
              </ion-col>
            </ion-row>
            <ion-row>
              <ion-col>
                Operador: {{element.operador}}
              </ion-col>
              <ion-col>
                <b>{{element.tipo == 0 ? 'Carga' : 'Descarga'}}</b>
              </ion-col>
            </ion-row>
          </ion-grid>
        </ion-card-content>
        <div [ngClass]="'cargaOdescarga'" ></div>
      </ion-card>
    </ion-item>
  </ion-list>
```
>Una vez que terminemos de agregar nuestor nuevo `<ion-card>` Agregaremos las variables y los metodos de nuestra API para que funcione

`Variables`
```
  public listaCargaDescarga: CargaDescarga[] = []
```
`Metodos`
```
  ionViewDidEnter(){
    //Cada vez que entremos a la pagina generara la busqueda.
    this.ngAfterViewInit();
  }

  ngAfterViewInit() {
    try {
      this.loadingCtrl.create({
        spinner:'circular',
        message:'Guardando ....',
      }).then(loading =>{
        //Mostramos el modal de carga
        loading.present();
        this.citisService.get().then((listaCargaDescarga:any)=>{
          if(this.utileriaService.validarWCF(listaCargaDescarga)){
            let respuesta = JSON.stringify(listaCargaDescarga.data);
  
            console.log(respuesta);
            this.listaCargaDescarga = JSON.parse(respuesta);
            loading.dismiss();
          }else{
            loading.dismiss();
          }
        })
      });

    } catch (error) {
      this.utileriaService.MessageBox("ERROR","Ocurrio un error fatal","alertDanger",0);
    }

  }
```
>Terminamos de anexar nuestra primera consulta GET de nuestra API. 

# API PASO 4

>El siguiente paso es para integrar las apis a nuestra Funcionalidad y comenzar a Guardar los Registros. 

En la siguiente metodo `Guardar()` dentro de la pagina `detalle.page.ts` Agregaremos el siguiente codigo.

```
  Guardar(){
    //Creamos un modal de Carga
    
    this.loadingCtrl.create({
      spinner:'circular',
      message:'Guardando ....',
    }).then(loading =>{
      //Mostramos el modal de carga
      loading.present();
      //Filtramos para enviar solamente las etiquetas agregadas despues de editar
      this.cargaDescarga.detalles = this.cargaDescarga.detalles.filter(x=>x.id == 0);

      this.citisService.post(this.cargaDescarga).then(cargaDescarga=>{ 
        if(this.utileriaService.validarWCF(cargaDescarga)){
          let respuesta = JSON.stringify(cargaDescarga);
          //Desaparecemos nuestro modal de carga
          loading.dismiss().then(()=>{
            //enviamos un mensaje de EXITO
            this.utileriaService.MessageBox("EXITO",`${this.tipo==0 ? 'Carga':'Descarga'} Guardado Correctamente `,"alertSuccess",0);

            //Cerramos  la pagina actual
            this.navCtrl.pop();
      
          });
        }else loading.dismiss();
      });
    });

  }

```
>Agregamos en nuestro `detalle.page.html` el siguiente codigo despues de la etiqueta `<ion-title></ion-title>`
```
    <ion-buttons slot="end">
      <ion-button (click)="Guardar()">
        <img class="icons" src="../../assets/img/disk.png">
      </ion-button>
    </ion-buttons>
```
>En este punto nuestra aplicacion es capas de Consultar informacion y enviar a la api registros que nosotros generemos para que los guarde en su base de datos. 

# API PASO 5
>Agregaremos una funcionalidad para CONSULTAR un registro en especifico a si nos mostrara solo dicho registro con sus respectivos de talles, para ellos es necesario agregar un metodo `modificar()` en el `home.page.ts` copiamos el siguiente codigo

```
  modificar(element:CargaDescarga){
    this.navCtrl.navigateForward("/detalle",{queryParams:{
      id:element.id, 
      operador: String(element.operador), 
      sucursal:  String(element.sucursal), 
      codigo_camion:  String(element.codigo_camion) ,
      tipo:element.tipo,
      isConsulta: true
    }});
  }
```
>para que nuestro metodo funcione es necesario en el `<ion-item>` agregarlo como atributo `(click)` de la siguiente manera

```
    <ion-item  *ngFor="let element of listaCargaDescarga; let i= index" (click) ="modificar(element)">

```
>A si es como debe de quedar el resultado. al hacer click en uno de nustros CARDS nos enviara a nuestra pagina de detalles para poder consultarlo.

# API PASO 6

>En `detalle.page.ts` agregamos despues del `constructor(){}`  el siguiente metodo si existe lo remplasamos.
```
  ngOnInit() {
    if(this.isConsulta){
      try {
            //Creamos un modal de Carga
      this.loadingCtrl.create({
        spinner:'circular',
        message:'Guardando ....',
      }).then(loading =>{
        //Mostramos el modal de carga
        loading.present();
        //Consultamos la carga por su ID
        this.citisService.getId(this.cargaDescarga.id).then((listaCargaDescarga:any)=>{
          if(this.utileriaService.validarWCF(listaCargaDescarga)){
            let respuesta = JSON.stringify(listaCargaDescarga.data);
  
            console.log(respuesta);
            //Asignamos a nuestra variable global la respuesta. 
            this.cargaDescarga = JSON.parse(respuesta);
            loading.dismiss();
          }else{
            loading.dismiss();
          }
        })
      });
  
      } catch (error) {
        this.utileriaService.MessageBox("ERROR","Ocurrio un error fatal","alertDanger",0);
      }
    }

  }

```

>En la misma pagina `detalle.page.ts` necesitamos modificar el metodo `Guardar()` ya que si te das cuenta utilizamos la misma pagina tanto para Generar el registro como modificarlo remplazaremos el  metodo de `Guardar()` por el siguiente codigo.

```

  Guardar(){
    //Creamos un modal de Carga
    this.loadingCtrl.create({
      spinner:'circular',
      message:'Guardando ....',
    }).then(loading =>{
      //Mostramos el modal de carga
      loading.present();
      //Filtramos para enviar solamente las etiquetas agregadas despues de editar
      this.cargaDescarga.detalles = this.cargaDescarga.detalles.filter(x=>x.id == 0);
      if(this.isConsulta){
        this.citisService.put(this.cargaDescarga).then(cargaDescarga=>{ 
          if(this.utileriaService.validarWCF(cargaDescarga)){
            //Desaparecemos nuestro modal de carga
            loading.dismiss().then(()=>{
              //enviamos un mensaje de EXITO
              this.utileriaService.MessageBox("EXITO",`${this.tipo==0 ? 'Carga':'Descarga'} Guardado Correctamente `,"alertSuccess",0);
  
              //Cerramos  la pagina actual
              this.navCtrl.pop();
        
            });
          }else loading.dismiss();
        });
      }else{
        this.citisService.post(this.cargaDescarga).then(cargaDescarga=>{ 
          if(this.utileriaService.validarWCF(cargaDescarga)){
            //Desaparecemos nuestro modal de carga
            loading.dismiss().then(()=>{
              //enviamos un mensaje de EXITO
              this.utileriaService.MessageBox("EXITO",`${this.tipo==0 ? 'Carga':'Descarga'} Guardado Correctamente `,"alertSuccess",0);
  
              //Cerramos  la pagina actual
              this.navCtrl.pop();
        
            });
          }else loading.dismiss();
        });
      }

    });

  }
```

>En este punto nuestra aplicacion es capas de consultar la informacion y modificar registros agregados.

# API PASO 7
>ELIMINAR PAQUETES. Necesitamos agregar funcionalidad al boton desplegable de los registros el boton llamado  `ELIMINAR` primero agregaremos al metodo `Eliminar()` la integracion de la API ELIMINAR, quedaria de la siguiente manera.

```
  Eliminar(element: CargaDescargaDetalle, index: number){
    this.utileriaService.MessageBoxYesNo("Eliminar","¿Deseas eliminar el registro "+element.codigo_producto+" de la captura de registros ?").then((bandera:boolean)=>{
      if(bandera){
        if(this.isConsulta){
          if(element.id == 0){ 
            this.utileriaService.MessageBox("ELIMINADO","Registro "+element.codigo_producto+" Eliminado Correctamente","alertSuccess",0);
            this.cargaDescarga.detalles.splice(index,1)
          }else{
            //Creamos un modal de Carga
            this.loadingCtrl.create({
              spinner:'circular',
              message:'Eliminando Registro ....',
            }).then(loading =>{
              //Mostramos el modal de carga
              loading.present();
              //eliminacion con api.
              this.citisService.delete(this.cargaDescarga.id,element.id).then(data=>{
                if(this.utileriaService.validarWCF(data)){
                  loading.dismiss();
                  this.utileriaService.MessageBox("ELIMINADO","Registro "+element.codigo_producto+" Eliminado Correctamente","alertSuccess",0);
                  this.cargaDescarga.detalles.splice(index,1)
                }else{
                  loading.dismiss();
                }
              });
            });
          }
        }else{
          //eliminacion directo al array
          this.utileriaService.MessageBox("ELIMINADO","Registro "+element.codigo_producto+" Eliminado Correctamente","alertSuccess",0);
          this.cargaDescarga.detalles.splice(index,1)
        }
      }
    });
  }
```

# INTEGRAR LECTOR QR

>npm install @capacitor-mlkit/barcode-scanning<br>
>npx cap sync

>Importamos las librerias de nuestro plugin QR `en detalle.page.ts`
```
import {
  BarcodeScanner,
  BarcodeFormat,
} from "@capacitor-mlkit/barcode-scanning";

import { Platform } from '@ionic/angular';

```
>Dentro del constructor agregamos el siguiente codigo 
```
constructor(
  ...
  public platform : Platform
){
  ....
  //button back cancelara el lector de codigo de barras.
  this.platform.backButton.subscribeWithPriority(10, () => {
    console.log('Handler was called!');
      BarcodeScanner.stopScan();
      document.querySelector('body')?.classList.remove('barcode-scanning-active');
  });
}
```
>Agregamos codigo despues de `ngOnInit(){}`
```
  async scan (){
    return new Promise(async resolve => {
      document.querySelector("body")?.classList.add("barcode-scanning-active");
      const listener = await BarcodeScanner.addListener(
        'barcodeScanned', 
        async (result:any) => {
          await listener.remove();
          await BarcodeScanner.stopScan();
          document.querySelector('body')?.classList.remove('barcode-scanning-active');

          console.log("Esto Arroja el Result", result);
          console.log("Esto Arroja el Result JSON", result.barcode.rawValue);

          this.agregarCaja(result.barcode.rawValue)
          resolve(true) 
        },
      );
      await BarcodeScanner.startScan();
    });
  }
```
>Integramos un icon con accion click para detonar nuestra funcion `scan(){}` en `detalle.page.html`<br>
>Lo integramos despues de nuestro input codigo
```
	<ion-icon (click)="scan()" slot="end" name="qr-code-outline"></ion-icon>
```


>Modificamos el Global CSS y Agregamos el siguiente Codigo

```
// Ocultar aplicacion para mostrar QR
  body.barcode-scanning-active {
    visibility: hidden;
    --background: transparent;
    --ion-background-color: transparent;
  }
  
  // Show only the barcode scanner modal
  .barcode-scanning-modal {
    visibility: visible;
  }
  
  @media (prefers-color-scheme: dark) {
    .barcode-scanning-modal {
      --background: transparent;
      --ion-background-color: transparent;
    }
  }
```



>Nuestra app ha Terminado !! :D

# GENERAR SPLASH E ICONOS APP

>Comando de generacion de splash e iconos 

>//CREACION DE SPLASH E ICONOS <br>

>npm install @capacitor/assets --save-dev <br>
>es necesario generar una carpeta llamada resources/  en raiz y agregar el icon.png 1024px x 1024px.y el splash.png 2732px x 2732px <br>
>Ejecutamos el siguiente comando. <br>
>npx capacitor-assets generate --android <br>

>Icon 1024px x 1024px. <br>
>Splash screen  2732px x 2732px. <br>
>Formato jpg o png. <br>


